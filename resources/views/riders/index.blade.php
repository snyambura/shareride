

@extends('/layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if(Session::has('success_msg'))
                <div class="alert alert-success">{{ Session::get('success_msg') }}</div>
            @endif
        <!-- Rides list -->
            @if(!empty($riders))
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Rides List </h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('riders.add') }}"> Add New</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <table class="table table-striped task-table">
                            <!-- Table Headings -->
                            <thead>
                            <th width="25%">Origin</th>
                            <th width="40%">Destination</th>
                            <th width="15%">Capacity</th>
                            <th width="20%">View</th>
                            </thead>

                            <!-- Table Body -->
                            <tbody>
                            @foreach($riders as $ride)
                                <tr>
                                    <td class="table-text">
                                        <div>{{$ride->origin}}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{$ride->destination}}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{$ride->capacity}}</div>
                                    </td>
                                    <td>
                                        <a href="{{ route('riders.details', $ride->id) }}" class="label label-success">View</a>
                                        {{--<a href="{{ route('riders.edit', $post->id) }}" class="label label-warning">Edit</a>--}}
                                        {{--<a href="{{ route('riders.delete', $post->id) }}" class="label label-danger" onclick="return confirm('Are you sure to delete?')">Delete</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Ride Details</h2>
            </div>
            <div class="pull-right">
                <a href="{{ route('riders.index') }}" class="label label-primary pull-right"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Origin:</strong>
                {{ $ride->origin }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Destination:</strong>
                {{ $ride->destination }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Capacity:</strong>
                {{ $ride->capacity }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <a href="{{ route('riders.details', $ride->id) }}" class="label label-success">Book</a>
        </div>


    </div>
@endsection
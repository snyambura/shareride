<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Riders extends Model
{
    //
    protected $fillable = ['origin', 'destination', 'capacity'];

    //timestamps
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Riders;

class RidersController extends Controller
{
    //
    public function index()
    {
        //fetch all rides
        $riders = Riders::orderBy('created','desc')->get();

        //pass rides data to view and load list view
        return view('riders.index', ['riders' => $riders]);
    }

    public function details($id)
    {
        //fetch post data
        $ride = Riders::find($id);

        //pass posts data to view and load list view
        return view('riders.details', ['ride' => $ride]);
    }
    public function add()
    {
        //load form view
        return view('riders.add');
    }
    public function insert(Request $request){
        //validate post data
        $this->validate($request, [
            'origin' => 'required',
            'destination' => 'required',
            'capacity' => 'required'
        ]);

        //get post data
        $rideData = $request->all();

        //insert post data
        Riders::create($rideData);

        //store status message
//        Session::flash('success_msg', 'Ride added successfully!');

        return redirect()->route('riders.index');
    }
}

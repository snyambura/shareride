<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/riders', 'RidersController@index')->name('riders.index');
Route::get('/riders/details/{id}', 'RidersController@details')->name('riders.details');
Route::get('/riders/add', 'RidersController@add')->name('riders.add');
Route::post('/riders/insert', 'RidersController@insert')->name('riders.insert');
